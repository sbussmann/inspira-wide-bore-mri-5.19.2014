##############################################################################
# Project: Inspira - MRI Wide Bore
# Date: 7/18/14
# Author: Sam Bussmann
# Description: Build multi-logistic model on payer types
# Notes: Establish relative value of different payers
##############################################################################

library(glmnet)

######### Rebuild the payer model based on the same design matrix used for propensity model

### get new cm

cm.payer<-cm[,2:dim(cm)[2]]

cm.payer$group.payer<-rep("Non-Patient",dim(cm.payer)[1])
cm.payer$group.payer[(pm$PayerType %in% 
                c("No Fault/Workers Comp","Managed Care","Blue Cross","Commercial","Other"))]<-"Commercial"
cm.payer$group.payer[(pm$PayerType %in% 
                c("Medicaid","Medicaid Mgd Care"))]<-"Medicaid"
cm.payer$group.payer[(pm$PayerType %in% 
                c("Medicare"))]<-"Medicare"
cm.payer$group.payer[(pm$PayerType %in% 
                c("Employee","Indigent Care","Unknown"))]<-"Self Pay"

## Deal with Unknowns (Recategorize top groups)
# cm.payer$group.payer[(pm$Payer %in% 
#                 c("HUMANA ADV/PPO/POS","GEICO","AETNA STARMARK","TRI STAR",
#                   "KAISER SALT LAKE CITY","AETNA ASSURANT HEALTH","COFINITY/EMPLOYEE BENEFIT PLANS",
#                   "USAA","ESIS","MERITAIN HEALTH","COFINITY/PRINCIPAL FINANCIAL GP",
#                   "COFINITY"))]<-"Commercial"
# cm.payer$group.payer[(pm$Payer %in% 
#                 c("MEDICARE RAILROAD","PCIP"))]<-"Medicare"
# cm.payer$group.payer[(pm$Payer %in% 
#                 c("LONGMONT SURGICAL MISSION"))]<-"Self Pay"

table(cm.payer$group.payer,useNA="ifany")

system.time(
  dev.mod.payer<-sparse.model.matrix(as.factor(group.payer)~ . +Age:Gender - Income ,data=cm.payer,
                     contrasts.arg = lapply(cm.payer[,sapply(cm.payer, is.factor)], contrasts, contrasts=FALSE))
)

save(dev.mod.payer,file="dev.mod.payer.Rdata")
#load("dev.mod.payer.Rdata")

## Is dev.mod.payer the same as dev.mod?

dev.mod.payer.pts<-dev.mod.payer[cm.payer$group.payer!="Non-Patient",]
payer.pts<-cm.payer$group.payer[cm.payer$group.payer!="Non-Patient"]

foldid.payer <- sample(1:10, size = length(payer.pts), replace = TRUE)
save(foldid.payer,file="foldid.payer.RData")

library(parallel)
library(doSNOW)
library(foreach)
cl<-makeCluster(2) 
registerDoSNOW(cl)

system.time(
  payerm1<-cv.glmnet(dev.mod.payer.pts, payer.pts,
                     family="multinomial",type.measure="class",standardize=F,parallel=T,
                     foldid=foldid.payer,alpha=1)
)

system.time(
  payerm2.1<-cv.glmnet(dev.mod.payer.pts, payer.pts,
                     family="multinomial",type.measure="deviance",standardize=F,parallel=T,
                     foldid=foldid.payer,alpha=1)
)

system.time(
  payerm2.5<-cv.glmnet(dev.mod.payer.pts, payer.pts,
                       family="multinomial",type.measure="deviance",standardize=F,parallel=T,
                       foldid=foldid.payer,alpha=.5)
)

system.time(
  payerm2.0<-cv.glmnet(dev.mod.payer.pts, payer.pts,
                       family="multinomial",type.measure="deviance",standardize=F,parallel=T,
                       foldid=foldid.payer,alpha=0.01)
)

stopCluster(cl)

max(payerm1$cvm)
min(payerm2.1$cvm)
min(payerm2.5$cvm)
min(payerm2.0$cvm)


which(payerm1$cvm==min(payerm1$cvm))
which(payerm2.1$cvm==min(payerm2.1$cvm))
which(payerm2.5$cvm==min(payerm2.5$cvm))
which(payerm2.0$cvm==min(payerm2.0$cvm))


## Go with model payerm2.1

save(payerm2.1,file="payer_model.Rdata")
#load("payer_model.Rdata")
pred.payer<-predict(payerm2.1,dev.mod.payer, s = "lambda.min",type="response")

save(pred.payer,file="payer.pred.Rdata")
#load("payer.pred.Rdata")

par(mfrow=c(1,1))
plot(payerm2.1)
plot(payerm2.1$glmnet.fit)
plot(payerm2.1$glmnet.fit,xvar="dev")
plot(payerm2.1$glmnet.fit,xvar="lambda")

coef(payerm2.1,s = "lambda.min")$Medicare


## Quick checks


tempage<-ifelse(pm$ExactAge=="90+",90,pm$ExactAge)

library(boot)
temp<-cbind(as.numeric(tempage)[!is.na(tempage)],pred.payer[,3,1][!is.na(tempage)])
corr(temp)

