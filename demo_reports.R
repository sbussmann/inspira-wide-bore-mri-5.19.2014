##############################################################################
# Project: Inspira - MRI Wide Bore
# Date: 7/18/14
# Author: Sam Bussmann
# Description: Demographic Output
# Notes: 
##############################################################################

coeff<-as.data.frame(as.matrix(coef(cvnet1.p1,s = "lambda.min")))
coeff<-data.frame(Varname=row.names(coeff),Value=coeff[,"1"],OddsRatio=exp(coeff[,"1"]))
write.csv(coeff,file="coeff.csv")

### Race

mean(!is.na(pm$vendorethnicity))
mean(!is.na(pm$vendorethnicgroup))

#########################################################################################

# Best 10%, Worst 10%, Overall by Gender

library(gplots)

b<-10
qqp<-unique(quantile(tog$score,probs=seq.int(0,1, length.out=b+1)))
outp <- cut(tog$score, breaks=qqp, include.lowest=TRUE, labels=as.character(c(10:1)))

gender1<-as.numeric(table(pm$Gender[outp==1])/sum(table(pm$Gender[outp==1])))
gendero<-as.numeric(table(pm$Gender)/sum(table(pm$Gender)))
gender10<-as.numeric(table(pm$Gender[outp==10])/sum(table(pm$Gender[outp==10])))
gendf<-data.frame(Gender_Decile_1=gender1,Gender_Overall=gendero,Gender_Decile_10=gender10)
dimnames(gendf)[[1]]<-c("Female","Male","Unknown")

#round(gendf,3)*100

dev.off()
dev.new(width=10,height=10)
par(mar=c(7,5.625,3.75,3.75)+0.1)
par(xpd=TRUE)
Gender_Breakdown <- barplot2(as.matrix(gendf*100),names.arg=c("","","","","","","","",""),
                             main="Gender of Scored Persons",col=c("lightpink","lightblue",gray(.75)), yaxt="n",
                             ylab="Percent", ylim=c(0,60), legend.text=F,beside=T)
axis(2, at=c(0,10,20,30,40,50,60), labels=paste0(c(0,10,20,30,40,50,60),"%"), las=1)

# label on the bars themselves
mtext("Best 10%                           Overall                        Worst 10%",side=1,line=1)
text(Gender_Breakdown,as.matrix(gendf*100),
     paste0(round(c(gendf[[1]]*100,gendf[[2]]*100,gendf[[3]]*100),1),"%"),
     cex=0.6,pos=3)

# legend
legend(locator(1),inset=.05, c("Female","Male","Unknown"), fill=c("lightpink","lightblue",gray(.75)),
       horiz=T, bty="o")

#########################################################################################

# Best 10% and Worst 10% by Income

intop_d1 <- (pm$CommunityPersonID %in% tog$CommunityPersonID[outp==1] )
intop_d10 <- (pm$CommunityPersonID %in% tog$CommunityPersonID[outp==10] )


# income range of best 10% of scored persons
dev.off()
png("income_plot_v2.png",width=650,height=900)

par(mfrow=c(2,1))
par(xpd=F)
par(mai=c(1.2,1.2,1,1))
hist(as.numeric(pm$FindIncome[intop_d1]),xlim=c(0,250000),breaks=50,ylim=c(0,10000),
     xlab="Income", main="Income of Best 10% of Scored Persons", xaxt="n", yaxt="n",
     col="forestgreen")
# x-axis specifications
ticksx <-pretty(c(0,50000,100000,150000,200000,250000))
valuesx <- format(ticksx,big.mark=",",scientific=FALSE)
axis(1, at=ticksx, labels=paste0("$",sub("^\\s+", "",valuesx)),cex.axis=0.8,las=1)
# y-axis specifications
ticksy <-pretty(c(0,2000,4000,6000,8000,10000))
valuesy <- format(ticksy,big.mark=",",scientific=FALSE)
axis(2, at=ticksy, labels=valuesy,cex.axis=0.8,las=1)

# income range of worst 10% of scored persons
par(xpd=F)
par(mai=c(1.2,1.2,.5,1))
hist(as.numeric(pm$FindIncome[intop_d10]),xlim=c(0,250000),breaks=50,ylim=c(0,10000),
     xlab="Income",main="Income of Worst 10% of Scored Persons", xaxt="n", yaxt="n",
     col="forestgreen")
# x-axis specifications
axis(1, at=ticksx, labels=paste0("$",sub("^\\s+", "",valuesx)),cex.axis=0.8,las=1)
# y-axis specifications
axis(2, at=ticksy, labels=valuesy,cex.axis=0.8,las=1)
dev.off()
#########################################################################################

# Best 10% and Worst 10% by Age #

tempage<-ifelse(pm$ExactAge=="90+",90,pm$ExactAge)

# plot both graphs on top of each other

dev.off()
png("age_plot_v2.png",width=650,height=900)

par(mfrow=c(2,1))
# age range of best 10% of scored persons
par(xpd=F)
par(mai=c(1.2,1.2,1,.5))
hist(as.numeric(tempage[intop_d1]),xlim=c(18,90),breaks=40,ylim=c(0,4000),
     xlab="Age",main="Age of Best 10% of Scored Persons",col="lavender", yaxt="n")
# y-axis specifications
ticks <- pretty(c(0, 1000,2000,3000,4000))
values <- format(ticks,big.mark=",",scientific=FALSE)
axis(2, at=ticks, labels=values,cex.axis=0.8,las=1)

# age range of worst 10% of scored persons
hist(as.numeric(tempage[intop_d10]),xlim=c(18,90),breaks=40,ylim=c(0,4000),
     xlab="Age",main="Age of Worst 10% of Scored Persons",col="lavender", yaxt="n")
axis(2, at=ticks, labels=values,cex.axis=0.8,las=1)
dev.off()