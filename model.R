##############################################################################
# Project: Inspira - MRI Wide Bore
# Date: 7/18/14
# Author: Sam Bussmann
# Description: Build Model
# Notes: 
##############################################################################

library(glmnet)
library(randomForest)
library(party)
library(SGL)

## Undersample non cases

#nonhk<-data.frame(id=sample.int(length(cm$pt_flag),175*sum(cm$pt_flag)))
nonhk<-data.frame(id=sample.int(length(cm$pt_flag),length(cm$pt_flag)))
hk<-data.frame(id=which(cm$pt_flag==1))
allhk<-merge(nonhk,hk,by=c("id"),all=T)
save(allhk,file="allhk.RData")
#load("allhk.RData")
### fit model using SGL to attempt to deal with grouping

## Create data structure for SGL model
# system.time(
#   dev.nor<-model.matrix(pt_flag~. ,data=cm,
#                         contrasts.arg = lapply(cm[,sapply(cm, is.factor)], contrasts, contrasts=FALSE))
# )
# 
# 
# dev.nor.noint<-dev.nor[allhk$id,-1]
# 
# temp<-list(x=dev.nor.noint,y=cm$pt_flag[allhk$id])
# 
# ## Create index indicating group membership
# 
# index<-NULL
# for (i in 1:(dim(cm)[2]-1)) {
#   if (is.factor(cm[[i]])) {index<-c(index,rep(i,nlevels(cm[[i]])))} else {index<-c(index,i)}
# }
# 
# system.time(
#   m1_sgl<-cvSGL(temp,index,type="logit",standardize=F,verbose=T,nfold=5,alpha=0)
# )
# 
# plot(m1_sgl)
# min(m1_sgl$lldiff)
# save(m1_sgl,file="m1_sgl.RData")
# 
# mean((abs(m1_sgl$fit$beta[,which(m1_sgl$lambdas==min(m1_sgl$lambda))]) > .01))

### Doesn't work??
##m1_sgl$beta<-m1_sgl$fit$beta
##pred.sgl<-predictSGL(m1_sgl$fit,dev.nor[,-1], 10)
# 
# pred.sgl2<- 1/(1+exp(-((dev.nor[,-1] %*% m1_sgl$fit$beta[,10]) + m1_sgl$fit$intercepts[10])))
# 
# plot(pred.sgl2-mean(pred.sgl2),pred.drg-mean(pred.drg))

### Fit model using glmnet ###
library(parallel)
library(doSNOW)
library(foreach)

system.time(
  dev.mod<-sparse.model.matrix(as.factor(pt_flag)~ . + Age:Gender + findr.bin:Age + spouseindicator.bin:findr.bin
                               - Income ,data=cm,
                               contrasts.arg = lapply(cm[,sapply(cm, is.factor)], contrasts, contrasts=FALSE))
)


### Testing LASSO vs Ridge / Elastic Net

cl<-makeCluster(2) 
registerDoSNOW(cl)

#foldid <- sample(1:10, size = length(cm$pt_flag), replace = TRUE)
foldid <- sample(1:10, size = length(allhk$id), replace = TRUE)
system.time(
  cvnet1.p1<-cv.glmnet(dev.mod[allhk$id,],cm$pt_flag[allhk$id], family="binomial",type.measure="deviance",
                       standardize=F,parallel = TRUE,foldid = foldid, alpha = 1)
)
system.time(
  cvnet1.p5<-cv.glmnet(dev.mod[allhk$id,],cm$pt_flag[allhk$id], family="binomial",type.measure="deviance",
                       standardize=F,parallel = TRUE,foldid = foldid, alpha = .5)
)
system.time(
  cvnet1.p0<-cv.glmnet(dev.mod[allhk$id,],cm$pt_flag[allhk$id], family="binomial",type.measure="deviance",
                       standardize=F,parallel = TRUE,foldid = foldid, alpha = .01)
)

stopCluster(cl)

mean((abs(cvnet1.p1$glmnet.fit$beta[,which(cvnet1.p1$lambda==cvnet1.p1$lambda.min)]) > .05))
mean((abs(cvnet1.p5$glmnet.fit$beta[,which(cvnet1.p5$lambda==cvnet1.p5$lambda.min)]) > .05))
mean((abs(cvnet1.p0$glmnet.fit$beta[,which(cvnet1.p0$lambda==cvnet1.p0$lambda.min)]) > .05))

save(cvnet1.p1,file="model_cvnet1.p1.Rdata")
#load("model_cvnet1.p1.Rdata")
save(cvnet1.p5,file="model_cvnet1.p5.Rdata")
save(cvnet1.p0,file="model_cvnet1.p0.Rdata")

min(cvnet1.p1$cvm)
min(cvnet1.p5$cvm)
min(cvnet1.p0$cvm)

which(cvnet1.p1$cvm==min(cvnet1.p1$cvm))
which(cvnet1.p5$cvm==min(cvnet1.p5$cvm))
which(cvnet1.p0$cvm==min(cvnet1.p0$cvm))


par(mfrow = c(1, 1))
plot(cvnet1.p1)
plot(cvnet1.p5)
plot(cvnet1.p0)
plot(log(cvnet1.p1$lambda), cvnet1.p1$cvm, pch = 19, col = "red", xlab = "log(Lambda)", 
     ylab = cvnet1.p1$name)
points(log(cvnet1.p5$lambda), cvnet1.p5$cvm, pch = 19, col = "grey")
points(log(cvnet1.p0$lambda), cvnet1.p0$cvm, pch = 19, col = "blue")
legend("top", legend = c("alpha = 1", "alpha = .5", "alpha = 0"), pch = 19, 
       col = c("red", "grey", "blue"))

coef(cvnet1.p1,s = "lambda.min")



pred.prop<-predict(cvnet1.p1,dev.mod, s = "lambda.min",type="response")

save(pred.prop,file="pred.prop.Rdata")
#load("pred.prop.Rdata")
